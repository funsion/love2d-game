function love.conf(t)                    -- love2d的配置选项和参数文件
    t.window.title = "俄罗斯方块"        -- 程序窗口标题 (string)
    t.window.width =  400                -- 窗口宽度像素值 (number)
    t.window.height = 800                -- 窗口高度像素值 (number)
end